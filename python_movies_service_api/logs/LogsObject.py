
import pika

class Logs:
    def __init__(self, hostname):
        self.hostname = hostname

    def send(self, msg):
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(host=self.hostname))
        channel = self.connection.channel()
        channel.queue_declare(queue='my_queue')
        channel.basic_publish(exchange='',
                              routing_key='my_queue',
                              body=msg)
        print(" [x] Sent '{}'".format(msg))
        self.connection.close()

    def recieve(self):
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(host=self.hostname))
        channel = self.connection.channel()
        channel.queue_declare(queue='my_queue')
        def callback(ch, method, properties, body):
            print(" [x] Received %r" % body)
        channel.basic_consume(queue='my_queue',
                              on_message_callback=callback,
                              auto_ack=True)
        print(' [*] Waiting for logs. To exit press CTRL+C')
        channel.start_consuming()
