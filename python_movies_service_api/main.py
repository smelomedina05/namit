from app import app
from controllers.RolesApi import roles_api
from controllers.PersonsApi import persons_api
from controllers.MoviesApi import movies_api
from controllers.CastingApi import casting_api
from flask_swagger_ui import get_swaggerui_blueprint

# Register each api here
app.register_blueprint(roles_api)
app.register_blueprint(persons_api)
app.register_blueprint(movies_api);
app.register_blueprint(casting_api);

SWAGGER_URL = '/swagger'
API_URL = '/static/swagger.json'
SWAGGERUI_BLUEPRINT = get_swaggerui_blueprint(
    SWAGGER_URL,
    API_URL,
    config={
        'app_name': "MoviesApi"
    }
)

app.register_blueprint(SWAGGERUI_BLUEPRINT, url_prefix=SWAGGER_URL)

@app.errorhandler(404)
def not_found(error=None):
    message = {
        'status': 404,
        'message': 'Not Found: ' + request.url,
    }
    resp = jsonify(message)
    resp.status_code = 404

    return resp

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')
