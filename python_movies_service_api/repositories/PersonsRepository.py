import pymysql
from db_config import mysql

class PersonsRepository(object):
    def __init__(self):
        self.conn = mysql.connect()

    def add_person(self, name, phone, email):
        sql = "INSERT INTO persons(name, phone, email) VALUES('{}', '{}', '{}')".format(name, phone, email)
        cursor = self.conn.cursor()
        cursor.execute(sql)
        self.conn.commit()
        lastrowid = cursor.lastrowid
        cursor.close()
        return lastrowid

    def get_all_persons(self, page, pagesize, name):
        startat = page*pagesize
        cursor = self.conn.cursor(pymysql.cursors.DictCursor)
        if name:
            cursor.execute("SELECT id, name, phone, email FROM persons WHERE name LIKE %s LIMIT %s, %s", (name,startat, pagesize))
        else:
            cursor.execute("SELECT id, name, phone, email FROM persons LIMIT %s, %s", (startat, pagesize))

        rows = cursor.fetchall()
        cursor.close()
        return rows

    def get_person_by_id(self, id):
        cursor = self.conn.cursor(pymysql.cursors.DictCursor)
        cursor.execute("SELECT id, name, phone, email FROM persons WHERE id=%s", id)
        row = cursor.fetchone()
        cursor.close()
        return row

    def update_person(self, id, name=None, phone=None, email=None):
        general = ""
        if name != None: general += "name='"+name+"', "
        if phone != None: general += "phone='"+phone+"', "
        if email != None: general += "email='"+email+"'"
        if general != "":
            sql = "UPDATE persons SET {} WHERE id={}".format(general, id)
            cursor = self.conn.cursor()
            rows_affected = cursor.execute(sql)
            self.conn.commit()
            cursor.close()
            return rows_affected
        else:
            return None

    def delete_person(self, id):
        cursor = self.conn.cursor()
        rows_affected = cursor.execute("DELETE FROM persons WHERE id=%s", (id,))
        self.conn.commit()
        cursor.close()
        return rows_affected
