import pymysql
from db_config import mysql

class CastingRepository(object):
    def __init__(self):
        self.conn = mysql.connect()

    def add_cast(self, id_person, id_role, id_movie, character):
        sql = "INSERT INTO full_cast_by_movie(`id_person`, `id_role`,`id_movie`, `character`) VALUES({}, {}, {}, '{}')".format(id_person, id_role, id_movie, character)
        print(sql)
        cursor = self.conn.cursor()
        cursor.execute(sql)
        self.conn.commit()
        lastrowid = cursor.lastrowid
        cursor.close()
        return lastrowid

    def get_all_casting(self, page, pagesize):
        startat = page*pagesize
        cursor = self.conn.cursor(pymysql.cursors.DictCursor)
        cursor.execute("SELECT `id_person`, `id_role`,`id_movie`, `character` FROM full_cast_by_movie LIMIT %s, %s", (startat, pagesize))
        rows = cursor.fetchall()
        cursor.close()
        return rows

    def get_cast_by_id(self, id_movie):
        cursor = self.conn.cursor(pymysql.cursors.DictCursor)
        cursor.execute("SELECT `id_person`, `id_role`,`id_movie`, `character` FROM full_cast_by_movie WHERE id_movie=%s", id_movie)
        row = cursor.fetchone()
        cursor.close()
        return row

    def delete_cast(self, id_movie):
        cursor = self.conn.cursor()
        rows_affected = cursor.execute("DELETE FROM full_cast_by_movie WHERE id_movie=%s ", (id_movie,))
        self.conn.commit()
        cursor.close()
        return rows_affected
