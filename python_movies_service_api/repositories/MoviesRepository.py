import pymysql
from db_config import mysql

class MoviesRepository(object):
    def __init__(self):
        self.conn = mysql.connect()

    def add_movie(self, name, description, stars, year):
        sql = "INSERT INTO movies(name, description, stars, year) VALUES('{}', '{}', '{}', '{}')".format(name, description, stars, year)
        cursor = self.conn.cursor()
        cursor.execute(sql)
        self.conn.commit()
        lastrowid = cursor.lastrowid
        cursor.close()
        return lastrowid

    def get_all_movies(self, page, pagesize, name):
        startat = page*pagesize
        cursor = self.conn.cursor(pymysql.cursors.DictCursor)
        if name:
            cursor.execute("SELECT id, name, description, stars, year FROM movies WHERE name LIKE %s LIMIT %s, %s", (name,startat, pagesize))
        else:
            cursor.execute("SELECT id, name, description, stars, year FROM movies LIMIT %s, %s", (startat, pagesize))

        rows = cursor.fetchall()
        cursor.close()
        return rows

    def get_movie_by_id(self, id):
        cursor = self.conn.cursor(pymysql.cursors.DictCursor)
        cursor.execute("SELECT id, name, description, stars, year FROM movies WHERE id=%s", id)
        row = cursor.fetchone()
        cursor.close()
        return row

    def update_movie(self, id, name, description, stars, year):
        general = ""
        if name != None: general += "name='"+ name + "'"
        if description != None: general += (", " if name != None else "") + "description='" + description + "'"
        if stars != None: general += (", " if description != None or name != None else "") + "stars='" + str(stars) + "'"
        if year != None: general += (", " if description != None or name != None or stars != None else "") + "year='"+ str(year) + "'"
        if general != "":
            print(general)
            sql = "UPDATE movies SET {} WHERE id={}".format(general, id)
            print(sql)
            cursor = self.conn.cursor()
            rows_affected = cursor.execute(sql)
            self.conn.commit()
            cursor.close()
            return rows_affected
        else:
            return None

    def delete_movie(self, id):
        cursor = self.conn.cursor()
        rows_affected = cursor.execute("DELETE FROM movies WHERE id=%s", (id,))
        self.conn.commit()
        cursor.close()
        return rows_affected
