from flask import Blueprint
from app import app
from services.PersonsService import PersonsService
from flask import jsonify
from logs.LogsObject import Logs
from flask import flash, request
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    get_jwt_identity
)

persons_api = Blueprint('persons_api', __name__)

persons_service = PersonsService()

persons_logs = Logs("rabbitmq_server")

@persons_api.route('/person', methods=['POST'])
def add_person():
    persons_logs.send("POST Person")
    try:
        _json = request.json
        _name = _json['name']
        _phone = _json['phone']
        _email = _json['email']
        # validate the received values
        if _name and _phone and _email and request.method == 'POST':
            lastrowid = persons_service.add_person(_name, str(_phone), _email)
            resp = jsonify({'id': lastrowid})
            resp.status_code = 200
            return resp
        else:
            return not_found()
    except Exception as e:
        print(e)

@persons_api.route('/person', methods=['GET'])
@jwt_required
def get_all_persons():
    persons_logs.send("GET all Persons")
    try:
        page = request.args.get('page', default = 1, type = int)
        name = request.args.get('name', default = None, type = str)
        app.logger.info("page: " + str(page))

        pagesize = 2
        rows = persons_service.get_all_persons(page, pagesize, name)
        resp = jsonify(rows)
        resp.status_code = 200
        return resp
    except Exception as e:
        print(e)

@persons_api.route('/person/<int:id>', methods=['GET'])
@jwt_required
def get_person_by_id(id):
    persons_logs.send("GET Person by id")
    try:
        row = persons_service.get_person_by_id(id)
        resp = jsonify(row)
        resp.status_code = 200
        return resp
    except Exception as e:
        print(e)

@persons_api.route('/person/<int:id>', methods=['PUT'])
def update_person(id):
    persons_logs.send("PUT Person by id")
    try:
        _json = request.json
        _name = _json['name'] if 'name' in _json else None
        _phone = _json['phone'] if 'phone' in _json else None
        _email = _json['email'] if 'email' in _json else None
        # validate the received values
        if id and request.method == 'PUT':
            rows_affected = persons_service.update_person(id, _name, _phone, _email)
            app.logger.info("PUT update_person, rows_affected: " + str(rows_affected))
            if rows_affected == 0:
                resp = jsonify({'message': 'Person was not updated!'})
                resp.status_code = 200
            else:
                resp = jsonify({'message': 'Person updated successfully!'})
                resp.status_code = 200
            return resp
        else:
            return not_found()
    except Exception as e:
        print(e)

@persons_api.route('/person/<int:id>', methods=['DELETE'])
def delete_person(id):
    persons_logs.send("DELETE Person by id")
    try:
        rows_affected = persons_service.delete_person(id)
        if rows_affected == 0:
            resp = jsonify({'message': 'Person was not deleted!'})
            resp.status_code = 200
        else:
            resp = jsonify({'message': 'Person deleted successfully!'})
            resp.status_code = 200
        return resp
    except Exception as e:
        print(e)

@persons_api.errorhandler(404)
def not_found(error=None):
    message = {
        'status': 404,
        'message': 'Not Found: ' + request.url,
    }
    resp = jsonify(message)
    resp.status_code = 404

    return resp
