from flask import Blueprint
from app import app
from services.CastingService import CastingService
from flask import jsonify
from flask import flash, request
from logs.LogsObject import Logs
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    get_jwt_identity
)

casting_api = Blueprint('casting_api', __name__)

casting_service = CastingService()

casting_logs = Logs("rabbitmq_server")

@casting_api.route('/cast', methods=['POST'])
def add_cast():
    casting_logs.send("POST Cast")
    try:
        _json = request.json
        _id_person = _json['id_person']
        _id_role = _json['id_role']
        _id_movie = _json['id_movie']
        _character = _json['character']
        # validate the received values
        if _id_person and _id_role and _id_movie and _character and request.method == 'POST':
            lastrowid = casting_service.add_cast(_id_person, _id_role, _id_movie, _character)
            resp = jsonify({'id': lastrowid})
            resp.status_code = 200
            return resp
        else:
            return not_found()
    except Exception as e:
        print(e)

@casting_api.route('/cast', methods=['GET'])
@jwt_required
def get_all_casting():
    casting_logs.send("GET all Casting")
    try:
        page = request.args.get('page', default = 1, type = int)
        name = request.args.get('name', default = None, type = str)
        app.logger.info("page: " + str(page))

        pagesize = 2
        rows = casting_service.get_all_casting(page, pagesize)
        resp = jsonify(rows)
        resp.status_code = 200
        return resp
    except Exception as e:
        print(e)

@casting_api.route('/cast/<int:id>', methods=['GET'])
@jwt_required
def get_cast_by_id(id):
    casting_logs.send("GET Cast by id")
    try:
        row = casting_service.get_cast_by_id(id)
        resp = jsonify(row)
        resp.status_code = 200
        return resp
    except Exception as e:
        print(e)

@casting_api.route('/cast/<int:id>', methods=['DELETE'])
def delete_cast(id):
    casting_logs.send("DELETE Cast by id")
    try:
        rows_affected = casting_service.delete_cast(id)
        if rows_affected == 0:
            resp = jsonify({'message': 'Cast was not deleted!'})
            resp.status_code = 200
        else:
            resp = jsonify({'message': 'Cast deleted successfully!'})
            resp.status_code = 200
        return resp
    except Exception as e:
        print(e)

@casting_api.errorhandler(404)
def not_found(error=None):
    message = {
        'status': 404,
        'message': 'Not Found: ' + request.url,
    }
    resp = jsonify(message)
    resp.status_code = 404

    return resp
