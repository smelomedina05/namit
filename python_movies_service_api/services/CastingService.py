from repositories.CastingRepository import CastingRepository

class CastingService(object):
    def __init__(self):
        self.casting_repository = CastingRepository()

    def add_cast(self, id_person, id_role, id_movie, character):
        return self.casting_repository.add_cast(id_person, id_role, id_movie, character)

    def get_all_casting(self, page, pagesize):
        return self.casting_repository.get_all_casting(page, pagesize)

    def get_cast_by_id(self, id):
        return self.casting_repository.get_cast_by_id(id)

    def delete_cast(self, id):
        return self.casting_repository.delete_cast(id)
