from repositories.PersonsRepository import PersonsRepository

class PersonsService(object):
    def __init__(self):
        self.persons_repository = PersonsRepository()

    def add_person(self, name, phone, email):
        return self.persons_repository.add_person(name, phone, email)

    def get_all_persons(self, page, pagesize, name):
        return self.persons_repository.get_all_persons(page, pagesize, name)

    def get_person_by_id(self, id):
        return self.persons_repository.get_person_by_id(id)

    def update_person(self, id, name, phone, email):
        return self.persons_repository.update_person(id, name, phone, email)

    def delete_person(self, id):
        return self.persons_repository.delete_person(id)
